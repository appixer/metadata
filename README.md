# appixer-metadata

**appixer-metadata** is a python package for updating metadata of FLAC and MPC. It is a wrapper of [mutagen].

## Getting Started

```
pip install appixer-metadata
```

[mutagen]: https://pypi.org/project/mutagen/
